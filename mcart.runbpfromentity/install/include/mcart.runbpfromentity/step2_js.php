<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
if ((CModule::IncludeModule('bizproc'))&&(CModule::IncludeModule("crm")))
  {
  
	$E_TYPE = (isset($_SESSION["MCART_ENTITY_TYPE"]) ? $_SESSION["MCART_ENTITY_TYPE"] : "deal");
	$LastDealID = (isset($_SESSION["MCART_CURRENT_DEAL_ID"]) ? $_SESSION["MCART_CURRENT_DEAL_ID"] : 1);
		if ($E_TYPE=='deal')
		{
			
			$entityType = "CCrmDocumentDeal";
			$docType = "DEAL";
			$dealLIST = CCrmDeal::GetList(array('ID' => 'ASC'), array(">ID"=>$LastDealID), array("ID", "TITLE"), $_SESSION["MCART_PROCESS_DEAL_STEP"]);
		}
		elseif($E_TYPE=='company')	
		{
			$entityType = "CCrmDocumentCompany";
			$docType = "COMPANY";
			$dealLIST = CCrmCompany::GetList(array('ID' => 'ASC'), array(">ID"=>$LastDealID), array("ID", "TITLE"), $_SESSION["MCART_PROCESS_DEAL_STEP"]);
		}
		elseif($E_TYPE=='contact')	
		{
			$entityType = "CCrmDocumentContact";
			$docType = "CONTACT";
			$dealLIST = CCrmContact::GetList(array('ID' => 'ASC'), array(">ID"=>$LastDealID), array("ID", "NAME"), $_SESSION["MCART_PROCESS_DEAL_STEP"]);
		}
		elseif($E_TYPE=='lead')	
		{
			$entityType = "CCrmDocumentLead";
			$docType = "LEAD";
			$dealLIST = CCrmLead::GetList(array('ID' => 'ASC'), array(">ID"=>$LastDealID), array("ID", "TITLE"), $_SESSION["MCART_PROCESS_DEAL_STEP"]);
		}
			$dealID = false;
			
			
				while ($obDEAL = $dealLIST->GetNext())
					{
					$dealID = $obDEAL["ID"];

					$arErrorsTmp = array();
					$arBizProcParametersValues = array();

							$res = CBPDocument::StartWorkflow(
										$_SESSION["MCART_CURRENT_BIZPROC_ID"],
										array('crm', $entityType, $docType.'_'.$dealID),
										$arBizProcParametersValues,
										$arErrorsTmp
									);
					}
					
			if ($dealID)		
				{echo  $dealID;	$_SESSION["MCART_CURRENT_DEAL_ID"] = $dealID;}
			else
				{echo "The End"; exit();}
			
		
			
	}	
?>			