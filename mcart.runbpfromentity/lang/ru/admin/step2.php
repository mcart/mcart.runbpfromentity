<?
$MESS ["MCART_FINISH_OK"] = "Процесс благополучно завершен";
$MESS["MCART_SELECT_DEAL_BP_TEMPLATE"] = "Выберите БП";
$MESS["MCART_NO_DEALS_TEMPLATE_FOUND"] = "не найдено ни одного БП";
$MESS["MCART_BIZPROC_MODULE_NOT_INSTALLED"] = "Не установлены модули bizproc и(или) crm";
$MESS["MCART_PUT_BIZPROC"] = "Начать процесс";
$MESS["MCART_TEST_BUTTON"] = "Тестовый запуск для одной";
$MESS["MCART_SELECT_ONE_DEAL"] = "ID:";
$MESS["MCART_ONE_MOMENT_PLEASE"] = "Подождите, загружаются данные для обработки...";
$MESS["MCART_WORKING_DEAL"] = "Обрабатывается сущность № ";
$MESS["MCART_FINISH_OK"] = "Процесс успешно завершен";
?>