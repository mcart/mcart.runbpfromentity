<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin.php");
IncludeModuleLangFile( __FILE__);
if ((CModule::IncludeModule('bizproc'))&&(CModule::IncludeModule("crm")))
	{
	$entityType = "CCrmDocumentContact";
	$docType = "CONTACT";
	
	
?>
	<?
	if ($y = $_POST['putBizproc']){
		$bpID = $_POST["bp_id"];
		$_SESSION["MCART_CURRENT_DEAL_ID"] = 0;
		$_SESSION["MCART_ENTITY_TYPE"] = "contact";
		$_SESSION["MCART_CURRENT_BIZPROC_ID"] = $bpID;
		$_SESSION["MCART_PROCESS_DEAL_STEP"] = $_POST["step"];
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.runbpfromentity/admin/step2.php");
		
		}
		else		
		{
		?>
			<form action="<?=$APPLICATION->GetCurPage()?>" method="POST" class="bizproc-form" name="start_crmdeal_bizproc" id="start_crmdeal_bizproc">
				<?
				$db_res = CBPWorkflowTemplateLoader::GetList(
				array($by => $order),
				array('DOCUMENT_TYPE' => array('crm', $entityType, $docType)),
				false,
				false,
				array('ID', 'NAME'));
				if ($db_res)
					{
					echo GetMessage("MCART_SELECT_BP_TEMPLATE");
					?>
					<select name = "bp_id" >
						<?while ($arTemplate = $db_res->Fetch()):?>
							<option value="<?=$arTemplate["ID"]?>"><?=$arTemplate["NAME"]?></option>
						<?endwhile;?>
					</select>
					</br>
					<?=GetMessage("MCART_STEP_COUNT")?><input type="text" name="step" value="20" />
					</br>
					<input type="submit" name="putBizproc" value="<?=GetMessage("MCART_PUT_BIZPROC")?>" />
					</br>
					</br>
					</br>
					</br>
					
					<?
					}
				else
					ShowError(GetMessage("MCART_NO_DEALS_TEMPLATE_FOUND"));
				?>
			</form>
		<?
		}
	}
else ShowError(GetMessage("MCART_BIZPROC_MODULE_NOT_INSTALLED"));		
?>
<?require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>