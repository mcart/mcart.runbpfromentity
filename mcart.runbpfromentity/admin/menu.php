<?

IncludeModuleLangFile(__FILE__);
$APPLICATION->SetAdditionalCSS("/bitrix/panel/main/mcart_runbpfromentity_menu.css");

if($APPLICATION->GetGroupRight("mcart.runbpfromentity")!="D"){
    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "section" => "mcart.runbpfromentity",
        "sort" => 800,
		"icon" => "mcart_runbpfromentity_menu_icon",
        "text" =>  GetMessage("MCART_RUNBPFROMENTITY"),
        "title" =>  GetMessage("MCART_RUNBPFROMENTITY"),
        
        
        "items_id" => "menu_mcart_runbpfromentity",
		 "items"       => array()
		
    );
	 $aMenu["items"][] =  array(
        "text" => GetMessage("MCART_RUNBPFROMDEAL"),
       
        "icon" => "mcart_runbpfromdeal_menu_icon",
        "page_icon" => "mcart_runbpfromdeal_menu_icon",
        
        "title" => GetMessage("MCART_RUNBPFROMDEAL"),
		
								"url"  => "mcart_runbpfromdeal.php?lang=".LANGUAGE_ID,
								"icon" => "mcart_runbpfromdeal_menu_icon",
								"page_icon" => "mcart_runbpfromdeal_menu_icon",
								
								"title" => GetMessage("MCART_RUNBPFROMDEAL"),
								"items"       => array()
       );
	   $aMenu["items"][] =  array(
        "text" => GetMessage("MCART_RUNBPFROMCOMPANY"),
       
        "icon" => "mcart_runbpfromcompany_menu_icon",
        "page_icon" => "mcart_runbpfromcompany_menu_icon",
        
        "title" => GetMessage("MCART_RUNBPFROMCOMPANY"),
		
								"url"  => "mcart_runbpfromcompany.php?lang=".LANGUAGE_ID,
								"icon" => "mcart_runbpfromcompany_menu_icon",
								"page_icon" => "mcart_runbpfromcompany_menu_icon",
								
								"title" => GetMessage("MCART_RUNBPFROMCOMPANY"),
								"items"       => array()
       );
	   $aMenu["items"][] =  array(
        "text" => GetMessage("MCART_RUNBPFROMLEAD"),
       
        "icon" => "mcart_runbpfromcompany_menu_icon",
        "page_icon" => "mcart_runbpfromcompany_menu_icon",
        
        "title" => GetMessage("MCART_RUNBPFROMLEAD"),
		
								"url"  => "mcart_runbpfromlead.php?lang=".LANGUAGE_ID,
								"icon" => "mcart_runbpfromlead_menu_icon",
								"page_icon" => "mcart_runbpfromcompany_menu_icon",
								
								"title" => GetMessage("MCART_RUNBPFROMLEAD"),
								"items"       => array()
       );
	   $aMenu["items"][] =  array(
        "text" => GetMessage("MCART_RUNBPFROMCONTACT"),
       
        "icon" => "mcart_runbpfromcompany_menu_icon",
        "page_icon" => "mcart_runbpfromcompany_menu_icon",
        
        "title" => GetMessage("MCART_RUNBPFROMCOMPANY"),
		
								"url"  => "mcart_runbpfromcontact.php?lang=".LANGUAGE_ID,
								"icon" => "mcart_runbpfromcontact_menu_icon",
								"page_icon" => "mcart_runbpfromcontact_menu_icon",
								
								"title" => GetMessage("MCART_RUNBPFROMCONTACT"),
								"items"       => array()
       );
	   
	   
    return $aMenu;
	
}
return false;


?>