<?IncludeModuleLangFile( __FILE__);?>
<div id="step5-progress-bar">
<?=GetMessage("MCART_ONE_MOMENT_PLEASE");?>
</div>
<?

?>
<script type="text/javascript">
	function repeat_export() {
		BX.ajax({
		url: '/bitrix/include/mcart.runbpfromentity/step2_js.php',
           data: {},
           method: 'POST',
           dataType: 'html',
           timeout: 500000,
           async: false,
           processData: true,
           scriptsRunFirst: true,
           emulateOnload: true,
           start: true,
           cache: false,
           onsuccess: function(data){
            //BX("step5-progress-bar").innerHTML = data;
				if (parseInt(data)>0) {
					BX("step5-progress-bar").innerHTML = '<?=GetMessage("MCART_WORKING_DEAL")?>' + data;
					setTimeout(repeat_export, 1000);
					
				}
				else {
					BX("step5-progress-bar").innerHTML = '<?=GetMessage("MCART_FINISH_OK")?>';
				}
           },
           onfailure: function(){

           }
		});
	}
	
	BX.ready(function (){
			setTimeout(repeat_export, 10);
	});
	
</script>
